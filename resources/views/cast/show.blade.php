@extends('adminlte.master')

@section('content')
    <div class="mt-3 ml-3">
        <h4> {{ 'Nama : '. $cast->nama }} </h4>
        <p> {{ 'Umur : '. $cast->umur . ' Tahun' }} </p>
        <p> {{$cast->bio}} </p>
    </div>
@endsection