@extends('adminlte.master')

@section('content')
    <div class="mt-3 ml-3">
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Cast List Table</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <!-- Session Succes -->
                @if(session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                <!-- Penutup Session Succes -->
                <a href="/cast/create" class="btn btn-primary mb-3">Create New Cast</a>
              <table class="table table-bordered">
                <thead>                  
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Nama</th>
                    <th>Umur</th>
                    <th>Bio</th>
                    <th style="width: 40px">Actions</th>
                  </tr>
                </thead>
                <tbody>
                    <!--looping pemanggilan table pada database-->
                    @forelse ($cast as $key => $value) 
                    <tr>
                        <td> {{ $key + 1 }} </td>
                        <td> {{ $value->nama }} </td>
                        <td> {{ $value->umur }} </td>
                        <td> {{ $value->bio }}</td>
                        <td style="display:flex">
                            <a href="/cast/{{$value->id}}" class="btn btn-info btn-sm">Show</a>
                            <a href="/cast/{{$value->id}}/edit" class="btn btn-default btn-sm">Edit</a>
                            <form action="/cast/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="delete" class="btn btn-danger btn-sm">
                            </form>
                        </td>
                    </tr>
                    @empty
                        <tr>
                            <td colspan="4" align="center">No Data</td>
                        </tr>                      
                    @endforelse 
                    <!--penutup looping-->
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
    </div>
@endsection